import { post } from 'aws-amplify/api';

const postContactUs = async (form) => {
    try {
        const requestOperation = post({
            apiName: 'contactUsEmail',
            path: '/contactUs',
            options: {
                body: form
            }
        })

        const response = await requestOperation.response

        console.log('POST call succeeded');
        console.log(response);
    }
    catch(e) {
        console.log('POST call failed: ', e);
    }
}

export default {
    postContactUs,
}

