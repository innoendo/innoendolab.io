import { Amplify } from "aws-amplify";
import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import amplifyConfig from "./amplifyconfiguration.json"

Amplify.configure(amplifyConfig);

const app = createApp(App);

app.use(createPinia());
app.use(router);

app.mount("#app");
