import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
      meta: {
        title: "Innoendo IT Solutions | Innovation Within"
      },
    },
    {
      path: "/about",
      name: "about",  
      component: () => import("../views/UnderConstructionView.vue"),
      meta: {
        title: "About Us | Innoendo IT Solutions"
      },
      
    },
    {
      path: "/faq",
      name: "faq",
      component: () => import("../views/UnderConstructionView.vue"),
      meta: {
        title: "FAQ | Innoendo IT Solutions"
      },
    },
    {
      path: "/services",
      name: "services",
      component: () => import("../views/UnderConstructionView.vue"),
      meta: {
        title: "Services | Innoendo IT Solutions"
      },
    },
    {
      path: "/portfolio",
      name: "portfolio",
      component: () => import("../views/UnderConstructionView.vue"),
      meta: {
        title: "Portfolio | Innoendo IT Solutions"
      },
    },
  ],
});

router.afterEach((to, from) => {
    document.title = to.meta.title;
});

export default router;
